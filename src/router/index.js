import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

import userEditForm from '../components/userEditForm.vue';
import usersList from '../components/usersList.vue';

import taxonomy from '../components/taxonomy.vue';
import menu from '../components/menu.vue';
import contentPage from '../components/contentPage.vue';

Vue.use(VueRouter)

  const routes = [
  { path: '/', name: 'Home', component: Home },
  { path: '/user/edit/:id?', name: 'userEditForm', component: userEditForm },
  { path: '/user/list/', name: 'usersList', component: usersList },

  { path: '/content/page/:id?', name: 'contentPage', component: contentPage },
  { path: '/taxonomy/:id?', name: 'taxonomy', component: taxonomy },
  { path: '/menu/:id?', name: 'menu', component: menu },

]

const router = new VueRouter({
  routes
})

export default router
