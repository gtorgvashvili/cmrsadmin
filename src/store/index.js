import Vue from 'vue'
import Vuex from 'vuex'

import fn from "../helpers/func";
import _ from 'lodash';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    indx: {},
    conf: {},
    cache: {},
    tmp: {},
    loader: { main: 0, form: 0, button: 0, },
    formData: {  },
  },
  getters: {
    getFromList: function (state) {
      return function (params) {
        if(!_.get(state, params.key) || !_.get(params,'val'))return {};
        params.field = params.field?params.field:'id';

        for (let i in _.get(state, params.key)){
          if( _.get(state, params.key+'.'+i+'.'+params.field) == params.val )return JSON.parse(JSON.stringify( _.get(state, params.key+'.'+i) ));
        }

        return {};

      }
    },
    filterListBy: function (state) {
      return function (params) {
        if(!_.get(state, params.key) || !_.get(params,'val'))return {};
        params.field = params.field?params.field:'id';

        let ret = [];
        for (let i in _.get(state, params.key)){
          if( _.get(state, params.key+'.'+i+'.'+params.field) == params.val )ret.push( JSON.parse(JSON.stringify( _.get(state, params.key+'.'+i) )) );
        }

        return ret;

      }
    },

    getStateItem: function (state) {
      return function (params) {
        return JSON.parse(JSON.stringify( _.get(state, params.key) ));
      }
    }
  },
  mutations: {
    setState : function (state, data) {
      // Validate data key & value not equal false
      if (! data.key || ! data.val) return false;
      Vue.set(state, data.key, data.val);
    },

    setStateDeep : function (state, data){
      // Validate data key & value not equal false
      if (! data.key ) return false;
      if(! data.val ) data.val = '';
      _.set(state, data.key, data.val);

      let stateVar = data.key.split(".")[0];
      Vue.set(state, stateVar, JSON.parse(JSON.stringify(state[stateVar])))
    },

    /// update list object or add new one
    setListOb: function (state, data) {
      //// formData , list (list name), id
      if(!data.formData || !data.formData.id || !data.listTitle )return false;

      if(!_.get(state, data.listTitle))this.commit('setStateDeep', {key: data.listTitle, val: [] });
      let list = _.get(state, data.listTitle);

      data.formData = JSON.parse(JSON.stringify(data.formData));

      for (var i in list) {
        if( parseInt(list[i].id) != parseInt(data.formData.id) )continue;
        Vue.set(list, i, data.formData);
        return true;
      }

      list.push(data.formData);

    },

    delListOb: function (state, data) {
      //// formData , list (list name), id
      if(!data.id || !data.listTitle || !_.get(state, data.listTitle))return false;

      let list = _.get(state, data.listTitle);

      for (var i in list) {
        if( parseInt(list[i].id) != parseInt(data.id) )continue;
        Vue.delete(list, i);
        return true;
      }

      return false;

    },



  },

  actions: {

    getUsers: async function(context){
      if(context.state.cache.users)return false;
      let res = await fn.postData('user/getUsers', this.fd);
      // console.log(res);
      Vue.set(context.state.cache, 'users', res);
    },

    getPages: async function(context){
      if(context.state.cache.pages)return false;
      let res = await fn.postData('page/getPages', this.fd);
      // console.log(res);
      if(res.error)return false;

      Vue.set(context.state.cache, 'pages', res);
    },

    /// get taxonomy terms
    getTerms: async function(context){
      if(context.state.cache.pages)return false;
      let res = await fn.postData('taxonomy/getTerms', this.fd);
      // console.log(res);
      if(res.error)return false;
      Vue.set(context.state.cache, 'terms', res);
    },



    indx: async function (context) {
      context.state.indx = await fn.postData('map');
    }

  },

  modules: {

  }
})
