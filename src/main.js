import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import fn from './helpers/func';
Vue.prototype.$fn = fn;

import _ from 'lodash'
Vue.prototype._ = _;

Vue.use(require('vue-cookies'))

Vue.component('editor', require('./components/editor'));

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);


import '@/assets/css/frame.css'
import '@/assets/css/style.css'

// import '@riophae/vue-treeselect/dist/vue-treeselect.css'

// import axios from 'axios';
// Vue.prototype.$http = axios;

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
