import conf from "../helpers/conf";
import qs from 'qs';

export default {

    postData: async function (path = '', data = {}) {
        let response = this.req(path, data).then(data=>{
           return data;
           // return JSON.parse(data);
        });

        return response;

    },

    /** send request to server */
    req: async function (path = '', data = {}) {
        // Default options are marked with *
        // conf.apiUrl = 'http://localhost/ss.php?';
        const response = await fetch(conf.apiUrl+path, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
                // 'Content-Type': 'multipart/form-data; boundary=something',
                // 'Content-Type': 'text/plain; charset=UTF-8',
                'Content-Type': 'application/x-www-form-urlencoded',
                // "X-Requested-With": "XMLHttpRequest",
                'Accept': '*/*',
                // 'Content-Type': 'application/json',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: qs.stringify(data), // body data type must match "Content-Type" header
            // body: data // body data type must match "Content-Type" header
            // body: fd // body data type must match "Content-Type" header

        });
// console.log(response);
    if(response.ok){
        return response.json();
    }

    return false;
    },


    // manageFormErrors: function (feObject, results) {
    //
    // }

}


/** server .htaccess
 * # Disable directory browsing
 Options All -Indexes

 # ----------------------------------------------------------------------
 # Rewrite engine
 # ----------------------------------------------------------------------

 # Turning on the rewrite engine is necessary for the following rules and features.
 # FollowSymLinks must be enabled for this to work.
 <IfModule mod_rewrite.c>
 Options +FollowSymlinks
 RewriteEngine On
 SetEnv CI_ENVIRONMENT development
 # If you installed CodeIgniter in a subfolder, you will need to
 # change the following line to match the subfolder you need.
 # http://httpd.apache.org/docs/current/mod/mod_rewrite.html#rewritebase
 # RewriteBase /

 # Redirect Trailing Slashes...
 RewriteCond %{REQUEST_FILENAME} !-d
 RewriteRule ^(.*)/$ /$1 [L,R=301]

 # Rewrite "www.example.com -> example.com"
 RewriteCond %{HTTPS} !=on
 RewriteCond %{HTTP_HOST} ^www\.(.+)$ [NC]
 RewriteRule ^ http://%1%{REQUEST_URI} [R=301,L]

 # Checks to see if the user is attempting to access a valid file,
 # such as an image or css document, if this isn't true it sends the
 # request to the front controller, index.php
 RewriteCond %{REQUEST_FILENAME} !-f
 RewriteCond %{REQUEST_FILENAME} !-d
 RewriteRule ^(.*)$ index.php/$1 [L]

 # Ensure Authorization header is passed along
 RewriteCond %{HTTP:Authorization} .
 RewriteRule .* - [E=HTTP_AUTHORIZATION:%{HTTP:Authorization}]
 </IfModule>

 <IfModule !mod_rewrite.c>
 # If we don't have mod_rewrite installed, all 404's
 # can be sent to index.php, and everything works as normal.
 ErrorDocument 404 index.php
 </IfModule>

 # Disable server signature start
 ServerSignature Off
 # Disable server signature end
 */