module.exports = {
  root: true,
  env: {
    node: true
  },
  'extends': [
    'plugin:vue/essential',
    'eslint:recommended'
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  rules: {
    'no-console': 'off',
    "no-unused-vars": "off",
    'no-debugger': 'off',
    'vue/no-unused-components': 'off',
    'vue/no-unused-vars': 'off'
  }
}
